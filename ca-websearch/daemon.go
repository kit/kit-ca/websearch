package main

import (
	_ "encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/http/fcgi"
	"net/http/httputil"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"github.com/gofrs/uuid"
	"github.com/gorilla/mux"
	_ "github.com/k0kubun/pp"
	. "gitlab.kit.edu/kit/kit-ca/websearch"
)

const (
	local = 1 << iota
	tcp
	unix
)

var (
	localaddr        string
	modeArg          string
	mode             int
	ccache           *CertCache
	certRepoDir      string
	webrootDir       string
	watcherDone      chan bool
	initialBatchDone = make(chan bool, 1)
	newFileChan      chan string
	allWatchers      map[int]*AttributeState
)

func init() {
	log.SetFlags(0)

	// flag parsing
	flag.StringVar(&modeArg, "mode", "local", "Mode of operation: local (local webserver), tcp (FCGI via localaddr) or unix (FCGI via UNIX socket at localaddr)")
	flag.StringVar(&localaddr, "localaddr", "localhost:8000", "Local bind address (local, tcp) oder filename of UNIX socket (unix)")
	flag.StringVar(&certRepoDir, "certdir", "Certificates", "Location of certificate files")
	flag.StringVar(&webrootDir, "webroot", "webroot", "Location of static web files")
	//flag.StringVar(&sessionKeyFile, "sessionkeyfile", ".ca-websearch-sessionkeys.json", "Location of secret key used to authenticate and encrypt sessions")
	flag.Parse()
	modeArg = strings.TrimSpace(strings.ToLower(modeArg))
	switch modeArg {
	case "local":
		mode = local
	case "tcp":
		mode = tcp
	case "unix":
		mode = unix
	default:
		log.Fatal("Unknown mode:", modeArg)
	}
}

func redirectHandler(w http.ResponseWriter, r *http.Request) {
	var (
		serial = mux.Vars(r)["serial"]
		dowhat = mux.Vars(r)["dowhat"]
	)

	requester := r.Header.Get("X-Forwarded-For")
	if requester == "" {
		requester = r.RemoteAddr
	}

	ca, err := GetIssuer(serial, ccache)
	if err == nil {
		url := BuildCertificateLink(RedirTemplates[dowhat], ca, serial)
		//log.Printf("Redirecting request %s from %s to %s", r.URL.String(), requester, url)
		http.Redirect(w, r, url, http.StatusFound)
	} else {
		u, _ := uuid.NewV4()
		uuid4 := u.String()
		log.Printf("[%s] unable to process request %s from %s: serial %s not in cache", uuid4, r.URL.String(), requester, serial)
		errormsg := "Invalid serial number " + serial + " (errorid " + uuid4 + ")"
		http.Error(w, errormsg, http.StatusBadRequest)
	}
}

func pubsearchHandler(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	query := r.Form.Get("q")
	filter := MakePublicSearchFilter(query, allWatchers[WatchVisibile])
	results := ccache.Filter(filter)
	sort.Sort(results)
	w.Header().Set("cache-control", "no-store")
	w.Header().Set("Content-Type", "application/json")
	w.Write(results.JSONString(allWatchers))
}

func searchHandler(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	query := r.Form.Get("q")
	filter := MakeInternalSearchFilter(query)
	results := ccache.Filter(filter)
	sort.Sort(results)
	w.Header().Set("cache-control", "no-store")
	w.Header().Set("Content-Type", "application/json")
	w.Write(results.JSONString(allWatchers))
}

func downloadHandler(w http.ResponseWriter, r *http.Request) {
	var (
		format = mux.Vars(r)["format"]
		serial = mux.Vars(r)["serial"]
	)
	cert := ccache.Get(serial)
	if cert == nil {
		u, _ := uuid.NewV4()
		uuid4 := u.String()
		log.Printf("[%s] unable to process request %s, serial %s not in cache", uuid4, r.URL.String(), serial)
		errormsg := "Invalid serial number " + serial + " (errorid " + uuid4 + ")"
		http.Error(w, errormsg, http.StatusBadRequest)
	}
	switch format {
	case "der":
		w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s.crt", serial))
		w.Header().Set("Content-Type", "application/pkix-cert")
		w.Write(cert.GetDER())
	case "pem":
		w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s.pem", serial))
		w.Header().Set("Content-Type", "application/x-pem-file")
		w.Write(cert.GetPEM())
	}
}

func pubDownloadHandler(w http.ResponseWriter, r *http.Request) {
	var (
		format = mux.Vars(r)["format"]
		serial = mux.Vars(r)["serial"]
	)
	cert := ccache.Get(serial)
	if cert == nil {
		u, _ := uuid.NewV4()
		uuid4 := u.String()
		log.Printf("[%s] unable to process request %s, serial %s not in cache", uuid4, r.URL.String(), serial)
		errormsg := "Invalid serial number " + serial + " (errorid " + uuid4 + ")"
		http.Error(w, errormsg, http.StatusBadRequest)
		return
	}
	// check if certificate is public
	if allWatchers[WatchVisibile].Is(cert.Serial, Public) == false {
		u, _ := uuid.NewV4()
		uuid4 := u.String()
		//log.Printf("[%s] certificate %s, serial %s not public", uuid4, r.URL.String(), serial)
		errormsg := "Certificate " + serial + " is not public (errorid " + uuid4 + "), authorization required for download."
		http.Error(w, errormsg, http.StatusUnauthorized)
		return
	}
	switch format {
	case "der":
		w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s.crt", serial))
		w.Header().Set("Content-Type", "application/pkix-cert")
		w.Write(cert.GetDER())
	case "pem":
		w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s.pem", serial))
		w.Header().Set("Content-Type", "application/x-pem-file")
		w.Write(cert.GetPEM())
	}
}

func emailtocertHandler(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]
	// retrieve all certs with that email
	results := ccache.IndexEmail.Get(email)
	// only valid certs
	results = results.Filter(
		func(c *SearchableCert) bool {
			return allWatchers[WatchValid].Is(c.Serial, Valid) && c.NotAfter.After(time.Now())
		})
	w.Header().Set("cache-control", "no-store")
	w.Header().Set("Content-Type", "application/json")
	w.Write(results.JSONString(allWatchers))
}

func main() {
	var err error

	// create watcher for new certificates
	allCertsDirectory := filepath.Join(certRepoDir, ".archive")
	log.Println("Starting filewatcher for directory", allCertsDirectory)
	ccache = CertArchiveWatcher(allCertsDirectory, initialBatchDone)
	<-initialBatchDone
	log.Println(ccache.Len(), "certificates have been loaded into the certificate cache.")

	// handle Validity change
	allWatchers = CreateAllWatchers(certRepoDir)

	// create http interface
	r := mux.NewRouter()

	// add redirect handler
	r.Path("/redirect/{dowhat:getcert|installcert|viewcert}/{serial:[0-9]+}").
		Methods("GET").
		HandlerFunc(redirectHandler)

	// add public search handler
	r.Path("/pubsearch/v1/json").
		Methods("GET").
		Queries("q", "").
		HandlerFunc(pubsearchHandler)

	// add internal search handler
	r.Path("/search/v1/json").
		Methods("GET").
		Queries("q", "").
		HandlerFunc(searchHandler)

	// add download handler
	r.Path("/pubdownload/{format:pem|der}/{serial:[0-9]+}").
		Methods("GET").
		HandlerFunc(pubDownloadHandler)

	// add internal download handler
	r.Path("/download/{format:pem|der}/{serial:[0-9]+}").
		Methods("GET").
		HandlerFunc(downloadHandler)

	// add emailtocert handler
	r.Path("/emailtocert/v1/{email:.+@.+}").
		Methods("GET").
		HandlerFunc(emailtocertHandler)

	// add request dumping handler
	r.Path("/dumpreq").
		Methods("GET").
		HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			requestDump, requestErr := httputil.DumpRequest(r, true)
			if requestErr != nil {
				log.Print(requestErr)
			}
			w.Header().Set("content-type", "text/plain")
			w.Write(requestDump)
		})

	// add handler for static files
	r.PathPrefix("/").
		Handler(http.FileServer(http.Dir(webrootDir)))

	// DEBUG: add notfound handler
	r.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestDump, requestErr := httputil.DumpRequest(r, true)
		if requestErr != nil {
			log.Print(requestErr)
		}
		w.Header().Set("content-type", "text/plain")
		w.Write(requestDump)
		http.NotFound(w, r)
	})

	log.Printf("Serving %s via %s", modeArg, localaddr)
	var listener net.Listener
	switch mode {
	case local:
		err = http.ListenAndServe(localaddr, r)
	case tcp:
		listener, err = net.Listen("tcp", localaddr)
		if err != nil {
			log.Fatal(err)
		}
		defer listener.Close()
		err = fcgi.Serve(listener, r)
	case unix:
		err = fcgi.Serve(nil, r)
	}
	if err != nil {
		log.Fatal(err)
	}
}
