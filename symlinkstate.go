package websearch

import (
	"sync"
)

// maps a certificate's serial to true if a symlink exists
type CertSymlinkState struct {
	State map[string]bool
	sync.RWMutex
}

// keep all connected symlink states plus their associated values
// in one place
type AttributeState struct {
	state []*CertSymlinkState
	value []int
}

// WatchForSymlinkChange assign a symlink path to a bitmask
type WatchForSymlinkChange struct {
	Path  string
	Value int
}

// NewAttributeState creates a new set of SymlinkStateWatchers that
// constitute a single value for the underlying certificate
func NewAttributeState(watchthis []WatchForSymlinkChange) *AttributeState {
	initialBatchDone := make(chan bool, len(watchthis))
	a := AttributeState{}
	for _, data := range watchthis {
		a.state = append(a.state, SymlinkStateWatcher(data.Path, initialBatchDone))
		a.value = append(a.value, data.Value)
	}
	// wait for all symlink watchers to complete initial readings
	for i := 0; i < len(watchthis); i++ {
		<-initialBatchDone
	}
	return &a
}

// return the bitwise-AND-sum of all symlinks that the serial is part of. returns
// 0 on no-match.
func (as *AttributeState) Get(serial string) int {
	var rval int
	for idx, state := range as.state {
		if state.In(serial) {
			rval |= as.value[idx]
		}
	}
	return rval
}

// returns true if the bitwise-AND-sum of all symlinks if serial is what
// XXX: semantik sinnvoll?
func (as *AttributeState) Is(serial string, what int) bool {
	for idx, code := range as.value {
		if code == what {
			return as.state[idx].In(serial)
		}
	}
	return false
}

// returns the sum of all links in all watched directories
func (as *AttributeState) Len() int {
	l := 0
	for _, s := range as.state {
		l += s.Len()
	}
	return l
}

func NewCertSymlinkState() *CertSymlinkState {
	return &CertSymlinkState{
		State: make(map[string]bool),
	}
}

func (cv *CertSymlinkState) Len() int {
	cv.RLock()
	l := len(cv.State)
	cv.RUnlock()
	return l
}

// In returns true if serial is included in cv
func (cv *CertSymlinkState) In(serial string) bool {
	cv.RLock()
	_, present := cv.State[serial]
	cv.RUnlock()
	return present
}

// Add adds serial to cv
func (cv *CertSymlinkState) Add(serial string) {
	cv.Lock()
	cv.State[serial] = true
	cv.Unlock()
}

// Remove adds serial to cv
func (cv *CertSymlinkState) Remove(serial string) {
	cv.Lock()
	delete(cv.State, serial)
	cv.Unlock()
}
