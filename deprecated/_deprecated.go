package websearch

type pkixelement struct {
	OID    asn1.ObjectIdentifier
	Prefix string
	Name   string
}

var (
	oidNames = map[string]pkixelement{
		"2.5.4.3":              pkixelement{asn1.ObjectIdentifier{2, 5, 4, 3}, "CN", "CommonName"},
		"1.2.840.113549.1.9.1": pkixelement{asn1.ObjectIdentifier{1, 2, 840, 113549, 1, 9, 1}, "email", "email"},
		"2.5.4.11":             pkixelement{asn1.ObjectIdentifier{2, 5, 4, 11}, "OU", "OrganizationalUnit"},
		"2.5.4.10":             pkixelement{asn1.ObjectIdentifier{2, 5, 4, 10}, "O", "Organization"},
		"2.5.4.7":              pkixelement{asn1.ObjectIdentifier{2, 5, 4, 7}, "L", "Locality"},
		"2.5.4.8":              pkixelement{asn1.ObjectIdentifier{2, 5, 4, 8}, "ST", "Province"},
		"2.5.4.6":              pkixelement{asn1.ObjectIdentifier{2, 5, 4, 6}, "C", "Country"},
	}
)

func oldCallout() {
	// ab hier callout
	var wg sync.WaitGroup
	calloutResults := make(chan string, 2)
	// call both CAs in parallel
	for _, ca := range [...]string{kitcag1, kitcag2} {
		wg.Add(1)
		go func(c string) {
			defer wg.Done()
			// timeout after 15 secs
			var netClient = &http.Client{
				Timeout: time.Second * 15,
			}
			// ask
			resp, err := netClient.Head(fmt.Sprintf(testCertURL, ca, serial))
			if err != nil {
				return
			}
			if resp.Header.Get("Content-Type") == "application/x-x509-email-cert" {
				calloutResults <- ca
			}
		}(ca)
	}
	// wait for callouts to return
	wg.Wait()
	close(calloutResults)
	// take first success and roll with it
	for thisCA := range calloutResults {
		return thisCA, nil
	}
}
