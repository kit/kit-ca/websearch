package websearch

import (
//"crypto/x509"
//"encoding/pem"
//"io/ioutil"
)

type CertReaderIterator struct {
	filenames []string
}

func (i *CertReaderIterator) Add(filenames ...string) {
	i.filenames = append(filenames, filenames...)
}

func (i *CertReaderIterator) Reset() {
	i.filenames = []string{}
}

func (i *CertReaderIterator) IsEmpty() bool {
	return len(i.filenames) == 0
}

func (i *CertReaderIterator) First() string {
	if len(i.filenames) == 0 {
		return ""
	}
	first := i.filenames[0]
	// TODO…
	return first // <- nope
}
