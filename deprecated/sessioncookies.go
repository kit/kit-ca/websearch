package websearch

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"github.com/gorilla/sessions"
	"io/ioutil"
	"log"
)

type sessionSecrets struct {
	Auth    string
	Encrypt string
}

var (
	sessionKeyFile string
	cookieStore    *sessions.CookieStore
)

func initializeSession(filename string) *sessions.CookieStore {
	var (
		ss  sessionSecrets
		err error
	)
	contents, err := ioutil.ReadFile(filename)
	if err == nil {
		err = json.Unmarshal(contents, &ss)
		if err == nil {
			log.Printf("Read session secrets from %s", filename)
			authkey, err := base64.StdEncoding.DecodeString(ss.Auth)
			if err != nil {
				log.Fatalf("Unable to decode authentication key from %s: %s", filename, err)
			}
			enckey, err := base64.StdEncoding.DecodeString(ss.Encrypt)
			if err != nil {
				log.Fatalf("Unable to decode encryption key from %s: %s", filename, err)
			}
			return sessions.NewCookieStore(authkey, enckey)
		} else {
			log.Fatalf("Unable to decode session secrets file %s: %s", filename, err)
		}
	} else {
		log.Printf("Unable to open existing session secrets (%s), creating new ones", err)
		keys := make([]byte, 64+64)
		_, err = rand.Read(keys)
		if err != nil {
			log.Fatalf("Unable to generate secret session keys:", err)
		}
		contents, err = json.MarshalIndent(sessionSecrets{
			base64.StdEncoding.EncodeToString(keys[0:31]),
			base64.StdEncoding.EncodeToString(keys[32:63])}, "", "  ")
		if err != nil {
			log.Fatalf("Unable to decode session secrets file %s: %s", filename, err)
		}
		err = ioutil.WriteFile(filename, contents, 0600)
		if err != nil {
			log.Fatalf("Unable to write new session secrets file to %s: %s", filename, err)
		}
		return sessions.NewCookieStore(keys[0:31], keys[32:63])
	}
	// needed to stop go compiler from complaining
	return sessions.NewCookieStore([]byte("foo"), []byte("bar"))
}

func main() {
	// read session secrets from file
	cookieStore = initializeSession(sessionKeyFile)
}
