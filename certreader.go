package websearch

import (
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"log"
)

// ReadCertificates reads alls x509 certificates from a list of input files. Errors are logged and skipped.
func ReadCertificates(filenames ...string) []*x509.Certificate {
	var (
		allcerts []*x509.Certificate = make([]*x509.Certificate, 0, len(filenames))
		block    *pem.Block
	)

	for _, filename := range filenames {
		// read certificate
		content, err := ioutil.ReadFile(filename)
		if err == nil {
			// extract all certificates
			for {
				block, content = pem.Decode(content)
				if block == nil {
					break
				}
				// process only certificates
				if block.Type == "CERTIFICATE" {
					certs, err := x509.ParseCertificates(block.Bytes)
					if err != nil {
						log.Println(err.Error())
						continue
					}
					for _, cert := range certs {
						allcerts = append(allcerts, cert)
					}
				}
			}
		} else {
			log.Printf("Error reading file %s: %s", filename, err)
		}
	}
	return allcerts
}
