package main

import (
	"bytes"
	"encoding/gob"
	"flag"
	"fmt"
	_ "github.com/k0kubun/pp"
	. "gitlab.kit.edu/kit/kit-ca/lib/certificatestats"
	"log"
	//. "gitlab.kit.edu/kit/kit-ca/lib/certificatestats"
	"github.com/boltdb/bolt"
)

var (
	certbucket = []byte("certificates")
)

func main() {
	flag.Parse()
	db, err := bolt.Open("my.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// create buckets
	db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(certbucket)
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		return nil
	})

	all := ReadCertificates(flag.Args()...)

	err = db.Update(func(tx *bolt.Tx) error {
		buf := new(bytes.Buffer)
		b := tx.Bucket([]byte(certbucket))
		for _, c := range all {
			buf.Reset()
			enc := gob.NewEncoder(buf)
			enc.Encode(c) // TODO: error checking
			err = b.Put(c.SerialNumber.Bytes(), buf.Bytes())
		}

		return nil
	})

}
