package heiko

import (
	"testing"
)

/* Question: when building an array of unknown size (upper
   bound known), how expensive (in terms of computation time)
   isappend()ing every insert vs. pre-allocation plus truncating?
   Answer: Approx. 2.5x.
*/

func BenchmarkPre1(b *testing.B)    { benchmarkPre(1024, b) }
func BenchmarkPre2(b *testing.B)    { benchmarkPre(65536, b) }
func BenchmarkPre3(b *testing.B)    { benchmarkPre(16777216, b) }
func BenchmarkAppend1(b *testing.B) { benchmarkAppend(1024, b) }
func BenchmarkAppend2(b *testing.B) { benchmarkAppend(65536, b) }
func BenchmarkAppend3(b *testing.B) { benchmarkAppend(16777216, b) }

func benchmarkPre(size int, b *testing.B) {
    array := AllocateTestArray(size)
	for n := 0; n < b.N; n++ {
        Preallocate(*array)
	}
}

func benchmarkAppend(size int, b *testing.B) {
    array := AllocateTestArray(size)
	for n := 0; n < b.N; n++ {
        Append(*array)
	}
}

func Preallocate(source []int) {
	dest := make([]int, len(source))
	for i := 0; i < len(source); i++ {
		dest[i] = source[i]
	}
}

func Append(source []int) {
	var dest []int
	for i := 0; i < len(source); i++ {
		dest = append(dest, source[i])
	}
}

func AllocateTestArray(size int) *[]int {
	array := make([]int, size)
	for idx, _ := range array {
		array[idx] = idx
	}
    return &array
}
