package main

import (
	"github.com/fsnotify/fsnotify"
	"github.com/k0kubun/pp"
	"log"
)

var (
	watcherDone chan bool
)

/*
func foo() {
    _, newFileChan, _ := websearch.NewDirectoryWatcher("watchme/")
	for {
		select {
		case newFile := <-newFileChan:
            pp.Print("X", newFile)
		}
	}
}
*/

func bar(path string) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}

	go func() {
		for {
			select {
			case event := <-watcher.Events:
				pp.Print(event)
			case <-watcherDone:
				return
			}
		}
	}()

	err = watcher.Add(path)
	if err != nil {
		log.Fatal(err)
	}

	// wait forever
	select {}
}

func main() {
	bar("watchme")
}
