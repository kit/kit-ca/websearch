package main

import (
	//"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"github.com/k0kubun/pp"
	"io/ioutil"
)

func main() {
	f, _ := ioutil.ReadFile("cmd_dumpcert/net-dashboard-1.tmn.scc.kit.edu-2019-07-09-8854737385717428272297305380.pem")
	block, _ := pem.Decode(f)
	certs, _ := x509.ParseCertificates(block.Bytes)
	//pkey := certs[0].PublicKey.(*rsa.PublicKey).N.BitLen()
	//pp.Print(pkey)

	pp.Print(certs[0])
}
