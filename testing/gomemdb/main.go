package main

import (
	_ "bytes"
	_ "encoding/gob"
	"flag"
	_ "fmt"
	"github.com/hashicorp/go-memdb"
	_ "github.com/k0kubun/pp"
	. "gitlab.kit.edu/kit/kit-ca/lib/certificatestats"
	"gitlab.kit.edu/kit/kit-ca/websearch"
	_ "log"
)

var (
	schema *memdb.DBSchema
)

func init() {
	flag.Parse()

	schema = &memdb.DBSchema{
		Tables: map[string]*memdb.TableSchema{
			"textcert": &memdb.TableSchema{
				Name: "textcert",
				Indexes: map[string]*memdb.IndexSchema{
					"id": &memdb.IndexSchema{
						Name:    "id",
						Unique:  true,
						Indexer: &memdb.StringFieldIndex{Field: "Serial"},
					},
					"hexserial": &memdb.IndexSchema{
						Name:    "hexserial",
						Indexer: &memdb.StringFieldIndex{Field: "HexSerial"},
					},
					"sigalg": &memdb.IndexSchema{
						Name:    "sigalg",
						Indexer: &memdb.StringFieldIndex{Field: "SignatureAlgorithm"},
					},
					"subject": &memdb.IndexSchema{
						Name:    "subject",
						Indexer: &memdb.StringFieldIndex{Field: "Subject"},
					},
					"issuer": &memdb.IndexSchema{
						Name:    "issuer",
						Indexer: &memdb.StringFieldIndex{Field: "Issuer"},
					},
					"keylength": &memdb.IndexSchema{
						Name:    "keylength",
						Indexer: &memdb.StringFieldIndex{Field: "Issuer"},
					},
				},
			},
		},
	}
}

func main() {
	all := ReadCertificates(flag.Args()...)

	// Create a new data base
	db, err := memdb.NewMemDB(schema)
	if err != nil {
		panic(err)
	}

	// Create a write transaction
	txn := db.Txn(true)

	for _, c := range all {
		cert := websearch.CertToSearchable(c)
		if err := txn.Insert("textcert", cert); err != nil {
			panic(err)
		}

	}

	// Commit the transaction
	txn.Commit()
}
