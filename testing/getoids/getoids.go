package main

import (
	_ "crypto/x509"
	"crypto/x509/pkix"
	"flag"
	"github.com/k0kubun/pp"
	. "gitlab.kit.edu/kit/kit-ca/lib/certificatestats"
)

func main() {
	flag.Parse()
	all := ReadCertificates(flag.Args()...)
	freq := make(map[string]int)

	for _, c := range all {
		var names []pkix.AttributeTypeAndValue
		names = append(names, c.Subject.Names...)
		names = append(names, c.Subject.ExtraNames...)
		for _, atv := range names {
			t := atv.Type.String()
			freq[t] += 1
		}
	}
	pp.Print(freq)
}
