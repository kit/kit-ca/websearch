package main

import (
	"flag"
	"github.com/k0kubun/pp"
	. "gitlab.kit.edu/kit/kit-ca/lib/certificatestats"
)

func main() {
	flag.Parse()
	all := ReadCertificates(flag.Args()...)

	for _, c := range all {
		pp.Print(CertToSearchable(c))
	}
}
