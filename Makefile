.PHONY: default getdeps

.DEFAULT_GOAL := default

getdeps:
	go get gitlab.kit.edu/kit/kit-ca/lib/certificatestats/ca-websearch...

default: *.go ca-websearch/*.go
	go install -v  -ldflags="-s -w" gitlab.kit.edu/kit/kit-ca/lib/certificatestats/ca-websearch
