package websearch

import (
	"crypto/x509"
	"strings"
	"sync"
)

// CertCache provides a map[string]*SearchableCert that is thread-safe
type CertCache struct {
	certs map[string]*SearchableCert
	sync.RWMutex
	IndexEmail CertIndex
}

// SCFilter functions implement a selection criteria for a SearchableCert
type SCFilter func(*SearchableCert) bool

func NewCertCache() CertCache {
	return CertCache{
		certs:      make(map[string]*SearchableCert),
		IndexEmail: NewCertIndex(),
	}
}

// Add a certificate to a CertCache, returns certificate as SearchableCert
// and a flag indicating if an existing entry was overwritten
func (cc *CertCache) Add(cert *x509.Certificate) (*SearchableCert, bool) {
	searchableCert := CertToSearchable(cert)

	cc.Lock()
	_, isPresent := cc.certs[searchableCert.Serial]
	cc.certs[searchableCert.Serial] = &searchableCert
	cc.Unlock()

	// add certificate to email index
	for _, email := range searchableCert.EmailAddresses {
		if FilterEncryptionEmail(&searchableCert) {
			cc.IndexEmail.Add(strings.ToLower(email), &searchableCert)
		}
	}

	return &searchableCert, isPresent
}

// Delete a certificate from a CertCache
func (cc *CertCache) Delete(serial string) {
	cc.Lock()
	delete(cc.certs, serial)
	cc.Unlock()
}

// Len returns the number of SearchableCerts in a CertCache
func (cc *CertCache) Len() int {
	cc.RLock()
	l := len(cc.certs)
	cc.RUnlock()
	return l
}

// Get retrieves a SearchableCert by serial number, return nil on failure
func (cc *CertCache) Get(serial string) *SearchableCert {
	cc.RLock()
	cert, present := cc.certs[serial]
	cc.RUnlock()
	if present {
		return cert
	} else {
		return nil
	}
}

// Filter returns all SearchableCerts that match the filter's criteria
func (cc *CertCache) Filter(filter SCFilter) Searchresults {
	var matches Searchresults

	cc.RLock()
	for _, c := range cc.certs {
		if filter(c) == true {
			matches = append(matches, c)
		}
	}
	cc.RUnlock()
	return matches
}
