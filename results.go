package websearch

import (
	"encoding/json"
	_ "github.com/k0kubun/pp"
	"html/template"
	"io"
	"log"
)

var (
	resultTemplate *template.Template
)

const (
	resultTemplateString = `<table class="searchresults">
<tbody>
{{range .}}
<tr class="searchresult">
    <td>{{ .Subject }}</td>
</tr>{{end}}
</tbody>
</table>`
)

func init() {
	resultTemplate = template.Must(template.New("results").Parse(resultTemplateString))
}

// an array of *SearchableCert that implements sort.
type Searchresults []*SearchableCert

func (r Searchresults) Len() int {
	return len(r)
}

func (r Searchresults) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

// sort results by type, subject, validity
func (r Searchresults) Less(i, j int) bool {
	switch {
	case r[i].Type < r[j].Type:
		return true
	case r[i].Type > r[j].Type:
		return false
	default:
		switch {
		case r[i].rawCertificate.Subject.CommonName < r[j].rawCertificate.Subject.CommonName:
			return true
		case r[i].rawCertificate.Subject.CommonName > r[j].rawCertificate.Subject.CommonName:
			return false
		default:
			return r[i].rawCertificate.NotBefore.Before(r[j].rawCertificate.NotBefore)
		}
	}
}

// Filter returns the subset of results that match filter
func (r Searchresults) Filter(filter SCFilter) Searchresults {
	filtered := make(Searchresults, len(r))
	matches := 0
	for _, cert := range r {
		if filter(cert) {
			filtered[matches] = cert
			matches++
		}
	}
	return filtered[:matches]
}

func (r Searchresults) JSONString(watchers map[int]*AttributeState) []byte {
	result := JSONShell{
		Results: make([]*JSONResult, len(r)),
	}
	for idx, c := range r {
		result.Results[idx] = c.JSONResult(watchers)
	}
	j, err := json.Marshal(result)
	if err != nil {
		log.Println("JSON encoding error:", err)
	}
	return j
}

func (r Searchresults) WriteHTML(w io.Writer) {
	resultTemplate.Execute(w, r)
}
