package websearch

import (
	"github.com/fsnotify/fsnotify"
	"github.com/k0kubun/pp"
	"io/ioutil"
	"log"
	"path/filepath"
	"regexp"
)

func foo() { pp.Print() }

const (
	SYMLINKCHANLEN = 4096
	ARCHIVECHANLEN = 16384
)

var (
	serialMatcher            = regexp.MustCompile("(?:([[:digit:]]{8,})\\.pem)")
	MagicInitialImportMarker = "Not an actual file"
	MagicInitialFileEvent    = fsnotify.Event{"Not an actual file", fsnotify.Chmod}
)

// CertArchiveWatcher reads all certificates from the archive directory and watches
// for the creation of new ones. It returns a *CertCache which is constantly updated
// and a chan bool that reads a true after the existing certificates have been read.
func CertArchiveWatcher(path string, initialBatchDone chan bool) *CertCache {
	ccache := NewCertCache()
	eventChan := make(chan *string, ARCHIVECHANLEN)
	showNew := false

	// concurrently add new files
	go func() {
		for newcert := range eventChan {
			// MagicInitialFileEvent received => existing certificates read
			if newcert == &MagicInitialImportMarker {
				initialBatchDone <- true
				showNew = true
				log.Printf("Read %d certificates from %s", ccache.Len(), path)
				continue
			}
			// parse new files
			for _, c := range ReadCertificates(*newcert) {
				if showNew {
					log.Printf("Added new certificate %s from %s", c.SerialNumber.Text(10), *newcert)
				}
				ccache.Add(c)
			}
		}
	}()

	// create fsnotify watcher
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatalf("Unable to create fsnotify watcher: %s", err)
	}
	err = watcher.Add(path)
	if err != nil {
		log.Fatalf("Unable to watch %s: %s", path, err)
	}
	log.Printf("Watching %s for new certificates", path)

	// read initial batch of certificates
	go func() {
		log.Println("Loading all certificates from", path)
		files, err := ioutil.ReadDir(path)
		if err != nil {
			log.Fatalf("Unable to open directory %s: %s", path, err)
		}
		for _, file := range files {
			if !file.IsDir() {
				fullname := filepath.Join(path, file.Name())
				eventChan <- &fullname
			}
		}
		eventChan <- &MagicInitialImportMarker
	}()

	// add new file on write event
	go func() {
		for {
			select {
			case event := <-watcher.Events:
				if event.Op&fsnotify.Write == fsnotify.Write {
					eventChan <- &event.Name
				}
			case err := <-watcher.Errors:
				log.Printf("Error watching path %s: %s", path, err)
			}
		}
	}()

	return &ccache
}

// SymlinkStateWatcher reads all symlinks from path and watches creation and deletion
// of symlinks. It returns a CertSymlinkState which is constantly updated and a chan bool
// that reads a true after the existing symlinks have been read (to prevent the API from
// return incomplete results).
func SymlinkStateWatcher(path string, initialBatchDone chan bool) *CertSymlinkState {
	eventChan := make(chan *fsnotify.Event, SYMLINKCHANLEN)
	symlinkState := NewCertSymlinkState()
	showNew := false

	// concurrently change symlink state
	go func() {
		for event := range eventChan {
			// MagicInitialFileEvent received => existing links read
			if event == &MagicInitialFileEvent {
				initialBatchDone <- true
				showNew = true
				log.Printf("Done reading %d inital symlinks for %s", symlinkState.Len(), path)
				continue
			}
			// split path and filename
			_, file := filepath.Split(event.Name)

			// skip .archive, it's a symlink back to the general archive
			// TODO: maybe test for symlink target and ignore directories?
			if file == ".archive" {
				continue
			}

			// extract serial number from filename
			serialmatch := serialMatcher.FindStringSubmatch(file)
			if len(serialmatch) != 2 {
				log.Printf("Unable to extract serial from filename: %s", file)
				continue
			}
			serial := serialmatch[1]

			// wait for changes
			switch {
			case event.Op&fsnotify.Remove == fsnotify.Remove:
				symlinkState.Remove(serial)
				if showNew {
					log.Printf("%s removed from %s", serial, path)
				}
			case event.Op&fsnotify.Create == fsnotify.Create:
				symlinkState.Add(serial)
				if showNew {
					log.Printf("%s added to %s", serial, path)
				}
			}
		}
	}()

	// create fsnotify watcher
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatalf("Unable to create fsnotify watcher: %s", err)
	}
	err = watcher.Add(path)
	if err != nil {
		log.Fatalf("Unable to watch %s: %s", path, err)
	}
	log.Printf("Watching %s for symlink changes", path)

	// read existing links
	go func() {
		files, err := ioutil.ReadDir(path)
		if err != nil {
			log.Fatalf("Unable to open directory %s: %s", path, err)
		}
		//pp.Print(files)
		for _, file := range files {
			if !file.IsDir() {
				fullname := filepath.Join(path, file.Name())
				eventChan <- &fsnotify.Event{fullname, fsnotify.Create}
			}
		}
		eventChan <- &MagicInitialFileEvent
	}()

	// watch path for changes
	go func() {
		for {
			select {
			case event := <-watcher.Events:
				eventChan <- &event
			case err := <-watcher.Errors:
				log.Printf("Error watching path %s: %s", path, err)
			}
		}
	}()

	return symlinkState
}
