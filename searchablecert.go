package websearch

import (
	"bytes"
	"crypto/md5"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"github.com/hreese/go-humanreltime"
	"gitlab.kit.edu/kit/kit-ca/lib/certificatestats"
	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
	_ "log"
	"path/filepath"
	"reflect"
	"regexp"
	"slices"
	"strings"
	"time"
	"unicode"
	"unicode/utf8"
)

var (
	nameFiller = map[string]bool{
		"de":  true,
		"la":  true,
		"los": true,
		"von": true,
		"van": true,
		"der": true,
		"dem": true,
		"ter": true,
		"ul":  true,
	}
	publicQuerySplitter = regexp.MustCompile("[[:space:],]+")
	ZeroPrefix          = regexp.MustCompile("^(?:0x)?0*([0-9a-fA-F]+)$")
)

const (
	WatchValid = 1 << iota
	WatchVisibile
)

// coarse Type
const (
	Gruppe = 1 << iota
	Benutzer
	Extern
	Server
	Pseudonym
)

var SectigoPersonalFilter = func(c *x509.Certificate) bool {
	sectigoIssuers := []string{
		"CN=GEANT Code Signing CA 4,O=GEANT Vereniging,C=NL",          // GEANT_Code_Signing_CA_4.pem
		"CN=GEANT Personal CA 4,O=GEANT Vereniging,C=NL",              // GEANT_Personal_CA_4.pem
		"CN=GEANT Personal ECC CA 4,O=GEANT Vereniging,C=NL",          // GEANT_Personal_ECC_CA_4.pem
		"CN=GEANT eScience Personal CA 4,O=GEANT Vereniging,C=NL",     // GEANT_eScience_Personal_CA_4.pem
		"CN=GEANT eScience Personal ECC CA 4,O=GEANT Vereniging,C=NL", // GEANT_eScience_Personal_ECC_CA_4.pem
	}

	return slices.Contains(sectigoIssuers, c.Issuer.String())
}

var NoCommonNameFilter = func(c *x509.Certificate) bool {
	return len(c.Subject.CommonName) == 0
}

func TypeToName(t int) string {
	switch t {
	case Gruppe:
		return "Gruppe"
	case Benutzer:
		return "Benutzer"
	case Extern:
		return "Extern"
	case Server:
		return "Server"
	case Pseudonym:
		return "Pseudonym"
	default:
		return "Unbekannt"
	}
}

// symlink states
const (
	Unknown = 0
	Valid   = 1 << iota
	Expired
	Revoked
	Public
	Private
)

func ValidityToName(v int) string {
	switch v {
	case Valid:
		return "valid"
	case Expired:
		return "expired"
	case Revoked:
		return "revoked"
	default:
		return "unknown"
	}
}

func VisibilityToName(v int) string {
	switch v {
	case Public:
		return "public"
	case Private:
		return "private"
	default:
		return "unknown"
	}
}

// start all known watchers in certRepoDir
func CreateAllWatchers(certRepoDir string) map[int]*AttributeState {
	var allWatcherData = map[int][]WatchForSymlinkChange{
		WatchValid: []WatchForSymlinkChange{
			{filepath.Join(certRepoDir, "Validity/Valid"), Valid},
			{filepath.Join(certRepoDir, "Validity/Expired"), Expired},
			{filepath.Join(certRepoDir, "Validity/Revoked"), Revoked},
		},
		WatchVisibile: []WatchForSymlinkChange{
			{filepath.Join(certRepoDir, "Visibility/Public"), Public},
			{filepath.Join(certRepoDir, "Visibility/Private"), Private},
		},
		//		WatchProfile: []WatchForSymlinkChange{
		//            {filepath.Join(certRepoDir , "Profile/802.1X_Client")          , 802.1X_Client          },
		//            {filepath.Join(certRepoDir , "Profile/802.1X_User")            , 802.1X_User            },
		//            {filepath.Join(certRepoDir , "Profile/Code_Signing")           , Code_Signing           },
		//            {filepath.Join(certRepoDir , "Profile/Domain_Controller")      , Domain_Controller      },
		//            {filepath.Join(certRepoDir , "Profile/Exchange_Server")        , Exchange_Server        },
		//            {filepath.Join(certRepoDir , "Profile/LDAP_Server")            , LDAP_Server            },
		//            {filepath.Join(certRepoDir , "Profile/Mail_Server")            , Mail_Server            },
		//            {filepath.Join(certRepoDir , "Profile/Mitarbeiter")            , Mitarbeiter            },
		//            {filepath.Join(certRepoDir , "Profile/OCSP_Responder")         , OCSP_Responder         },
		//            {filepath.Join(certRepoDir , "Profile/Radius_Server")          , Radius_Server          },
		//            {filepath.Join(certRepoDir , "Profile/RA_Operator")            , RA_Operator            },
		//            {filepath.Join(certRepoDir , "Profile/Shibboleth_IdP_SP")      , Shibboleth_IdP_SP      },
		//            {filepath.Join(certRepoDir , "Profile/Smartcard")              , Smartcard              },
		//            {filepath.Join(certRepoDir , "Profile/Smartcard_Encrypt")      , Smartcard_Encrypt      },
		//            {filepath.Join(certRepoDir , "Profile/Smartcard_Logon")        , Smartcard_Logon        },
		//            {filepath.Join(certRepoDir , "Profile/Smartcard_Sign")         , Smartcard_Sign         },
		//            {filepath.Join(certRepoDir , "Profile/Smartcard_SignAndLogon") , Smartcard_SignAndLogon },
		//            {filepath.Join(certRepoDir , "Profile/Student")                , Student                },
		//            {filepath.Join(certRepoDir , "Profile/User")                   , User                   },
		//            {filepath.Join(certRepoDir , "Profile/UserAuth")               , UserAuth               },
		//            {filepath.Join(certRepoDir , "Profile/UserEMail")              , UserEMail              },
		//            {filepath.Join(certRepoDir , "Profile/UserEncrypt")            , UserEncrypt            },
		//            {filepath.Join(certRepoDir , "Profile/UserSign")               , UserSign               },
		//            {filepath.Join(certRepoDir , "Profile/UserSignAndLogon")       , UserSignAndLogon       },
		//            {filepath.Join(certRepoDir , "Profile/UserSignAuth")           , UserSignAuth           },
		//            {filepath.Join(certRepoDir , "Profile/VoIP_Server")            , VoIP_Server            },
		//            {filepath.Join(certRepoDir , "Profile/VPN_Server")             , VPN_Server             },
		//            {filepath.Join(certRepoDir , "Profile/VPN_User")               , VPN_User               },
		//            {filepath.Join(certRepoDir , "Profile/Web_Server")             , Web_Server             },
		//            {filepath.Join(certRepoDir , "Profile/Webserver_MustStaple")   , Webserver_MustStaple   },
		//		},
	}
	var watchers = make(map[int]*AttributeState)
	for key, symlinkset := range allWatcherData {
		watchers[key] = NewAttributeState(symlinkset)
	}
	return watchers
}

type SearchableCert struct {
	Serial             string
	HexSerial          string
	SignatureAlgorithm string
	Subject            string
	Issuer             string
	KeyLength          int
	NotBefore          time.Time
	NotAfter           time.Time
	DNSNames           []string
	EmailAddresses     []string
	IPAddresses        []string
	CAGeneration       *string
	Type               int
	searchablestring   string
	pubsearchToken     []string
	rawCertificate     *x509.Certificate
	FingerprintSHA1    string
	FingerprintSHA256  string
	FingerprintMD5     string
}

func (c *SearchableCert) GetPEM() []byte {
	b := pem.Block{
		Type:    "CERTIFICATE",
		Headers: nil,
		Bytes:   c.rawCertificate.Raw,
	}
	return pem.EncodeToMemory(&b)
}

func (c *SearchableCert) GetDER() []byte {
	return c.rawCertificate.Raw
}

// Unicode transformation (via http://stackoverflow.com/questions/26722450/remove-diacritics-using-go)
func isMn(r rune) bool {
	return unicode.Is(unicode.Mn, r) // Mn: nonspacing marks
}

// Cleanup query
func CleanupQueryString(query string) string {
	// transform to lowercase
	query = strings.ToLower(query)
	// remove leading and trailing white space
	query = strings.TrimSpace(query)
	// DFN-Rules (removed for Sectigo and HARICA/TBD)
	//query = strings.Replace(query, "ä", "ae", -1)
	//query = strings.Replace(query, "ö", "oe", -1)
	//query = strings.Replace(query, "ü", "ue", -1)
	//query = strings.Replace(query, "ß", "ss", -1)
	// remove diacritics and the like
	unicodeSimplification := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC)
	result, _, _ := transform.String(unicodeSimplification, query)
	return result
}

// NeverMatch does not match any SearchableCert
func NeverMatch(c *SearchableCert) bool { return false }

// MakeInternalSearchFilter creates a SCFilter that based on a simple substring match
func MakeInternalSearchFilter(query string) SCFilter {
	cleaned := CleanupQueryString(query)
	token := tokenizeQuery(cleaned)

	// remove colons and leading zeros from strings that look like hex numbers
	for idx, t := range token {
		if strings.Count(t, ":") > 1 {
			t = strings.Replace(t, ":", "", -1)
			m := ZeroPrefix.FindStringSubmatch(t)
			if len(m) > 1 && len(m[1]) > 0 {
				token[idx] = m[1]
			}
		}
	}

	// return zero results on empty query
	if len(token) == 0 {
		return NeverMatch
	}

	// check if every token matches a certificate (token are combined with AND)
	return func(c *SearchableCert) bool {
		numMatches := 0
		for _, t := range token {
			if strings.Contains(c.searchablestring, t) {
				numMatches++
			}
		}
		return numMatches == len(token)
	}
}

func tokenizeQuery(query string) []string {
	lastQuote := rune(0)
	// helper function that remembers the last quotation char (no nesting)
	findSingleQuotedToken := func(c rune) bool {
		switch {
		case c == lastQuote:
			lastQuote = rune(0)
			return false
		case lastQuote != rune(0):
			return false
		case unicode.In(c, unicode.Quotation_Mark):
			lastQuote = c
			return false
		default:
			return unicode.IsSpace(c)
		}
	}
	// split query into token
	token := strings.FieldsFunc(query, findSingleQuotedToken)

	var ret []string
	for _, t := range token {
		// skip empty string
		if len(t) == 0 {
			continue
		}
		if len(t) > 1 {
			first, lenFirst := utf8.DecodeRuneInString(t)
			last, lenLast := utf8.DecodeLastRuneInString(t)
			// remove quotes
			if unicode.In(first, unicode.Quotation_Mark) && unicode.In(last, unicode.Quotation_Mark) {
				t = t[lenFirst : len(t)-lenLast]
			}
		}
		// only append non-empty strings
		if len(t) > 0 {
			ret = append(ret, t)
		}
	}
	return ret
}

func MakePublicSearchFilter(query string, visibilityWatcher *AttributeState) SCFilter {
	cleaned := CleanupQueryString(query)
	queryTokens := publicQuerySplitter.Split(cleaned, -1)
	return func(c *SearchableCert) bool {
		// exclude non-public certs
		if visibilityWatcher.Is(c.Serial, Public) == false {
			return false
		}

		// only match certificates that contain at least one email address
		if len(c.EmailAddresses) < 1 {
			return false
		}
		/* 2017-06-03: disabled, this excludes some legitimate email certs (for example 6912340332260791)
		// check if the KeyEncipherment KeyUsage bit is set
		if c.rawCertificate.KeyUsage&x509.KeyUsageKeyEncipherment != x509.KeyUsageKeyEncipherment {
			return false
		}
		*/
		// check if ExtendedKeyUsage contains EmailProtection
		foundEmailProtection := false
		for _, extension := range c.rawCertificate.ExtKeyUsage {
			if extension == x509.ExtKeyUsageEmailProtection {
				foundEmailProtection = true
			}
		}
		if !foundEmailProtection {
			return false
		}
		// filter RA-Ops
		if strings.HasPrefix(c.rawCertificate.Subject.CommonName, "PN") && (strings.Contains(c.rawCertificate.Subject.CommonName, "Teilnehmerservice") || strings.HasSuffix(c.rawCertificate.Subject.CommonName, "RA-Operator")) {
			return false
		}
		// check if DN equals query
		if cleaned == c.rawCertificate.Subject.CommonName {
			return true
		}
		// check if all parts of the query match any precomputed certificate part
		var numMatches int
		for _, token := range queryTokens {
			for _, certToken := range append(c.pubsearchToken, c.EmailAddresses...) {
				if strings.EqualFold(token, certToken) {
					numMatches++
					continue
				}
			}
		}
		// return true if all queryTokens matched something
		return len(queryTokens) == numMatches
	}
}

func FilterEncryptionEmail(c *SearchableCert) bool {
	// only match certificates that contain at least one email address
	if len(c.EmailAddresses) < 1 {
		return false
	}
	// check if the KeyEncipherment KeyUsage bit is set
	if c.rawCertificate.KeyUsage&x509.KeyUsageKeyEncipherment != x509.KeyUsageKeyEncipherment {
		return false
	}
	// check if ExtendedKeyUsage contains EmailProtection
	foundEmailProtection := false
	for _, extension := range c.rawCertificate.ExtKeyUsage {
		if extension == x509.ExtKeyUsageEmailProtection {
			foundEmailProtection = true
		}
	}
	if !foundEmailProtection {
		return false
	}
	// filter RA-Ops
	if strings.HasPrefix(c.rawCertificate.Subject.CommonName, "PN") && (strings.Contains(c.rawCertificate.Subject.CommonName, "Teilnehmerservice") || strings.HasSuffix(c.rawCertificate.Subject.CommonName, "RA-Operator")) {
		return false
	}
	return true
}

// DnToString turns a DistinguishedName into a readable string
// According to the relevant RFCs, there is no canonical form.
// The returned format is designed to be sortable and have good
// readability.
func DnToString(n pkix.Name) string {
	var parts []string
	// CN
	parts = append(parts, "CN="+n.CommonName)
	// email
	for _, elem := range n.Names {
		if reflect.DeepEqual(elem.Type, oidEmail) {
			parts = append(parts, "email="+elem.Value.(string))
		}
	}
	// OU
	for _, elem := range n.OrganizationalUnit {
		parts = append(parts, "OU="+elem)
	}
	// O
	for _, elem := range n.Organization {
		parts = append(parts, "O="+elem)
	}
	// L
	for _, elem := range n.Locality {
		parts = append(parts, "L="+elem)
	}
	// ST
	for _, elem := range n.Province {
		parts = append(parts, "ST="+elem)
	}
	// C
	for _, elem := range n.Country {
		parts = append(parts, "C="+elem)
	}

	return strings.Join(parts, ",")
}

// CertToSearchable converts an x509.Certificate into a SearchableCert
func CertToSearchable(c *x509.Certificate) SearchableCert {
	cert := SearchableCert{
		Serial:             c.SerialNumber.Text(10),
		HexSerial:          "0x" + c.SerialNumber.Text(16),
		SignatureAlgorithm: SignatureAlgorithmNames[c.SignatureAlgorithm],
		Subject:            DnToString(c.Subject),
		Issuer:             DnToString(c.Issuer),
		DNSNames:           c.DNSNames,
		EmailAddresses:     c.EmailAddresses,
		NotBefore:          c.NotBefore,
		NotAfter:           c.NotAfter,
		rawCertificate:     c,
	}
	for _, ip := range c.IPAddresses {
		cert.IPAddresses = append(cert.IPAddresses, ip.String())
	}
	switch key := c.PublicKey.(type) {
	case *rsa.PublicKey:
		cert.KeyLength = key.N.BitLen()
	default:
		cert.KeyLength = -1
	}
	if bytes.Compare(c.RawIssuer, RawIssuerG1) == 0 {
		cert.CAGeneration = &kitcag1
	} else if bytes.Compare(c.RawIssuer, RawIssuerG2) == 0 {
		cert.CAGeneration = &kitcag2
	} else if bytes.Compare(c.RawIssuer, RawIssuerSectigo) == 0 {
		cert.CAGeneration = &sectigo
	} else {
		cert.CAGeneration = &unknown
	}

	// add serials
	cert.FingerprintSHA1 = fmt.Sprintf("0x%x", sha1.Sum(c.Raw))
	cert.FingerprintSHA256 = fmt.Sprintf("0x%x", sha256.Sum256(c.Raw))
	cert.FingerprintMD5 = fmt.Sprintf("0x%x", md5.Sum(c.Raw))

	// build string used for simple search
	var buffer bytes.Buffer
	buffer.WriteString(cert.Serial)
	buffer.WriteString(" ")
	buffer.WriteString(cert.HexSerial)
	buffer.WriteString(" ")
	buffer.WriteString(cert.rawCertificate.Subject.CommonName)
	//buffer.WriteString(cert.Subject)
	buffer.WriteString(" ")
	buffer.WriteString(strings.Join(cert.DNSNames, " "))
	buffer.WriteString(strings.Join(cert.EmailAddresses, " "))
	buffer.WriteString(strings.Join(cert.IPAddresses, " "))
	buffer.WriteString(" ")
	buffer.WriteString(cert.FingerprintSHA256)
	buffer.WriteString(" ")
	buffer.WriteString(cert.FingerprintSHA1)
	buffer.WriteString(" ")
	buffer.WriteString(cert.FingerprintMD5)

	cert.searchablestring = strings.ToLower(buffer.String())

	// build array for public search
	token := make(map[string]bool)
	// add CN and all substrings
	token[strings.ToLower(cert.rawCertificate.Subject.CommonName)] = true
	for _, namepart := range strings.Fields(cert.rawCertificate.Subject.CommonName) {
		_, unwanted := nameFiller[namepart]
		if unwanted == false {
			token[strings.ToLower(namepart)] = true
		}
	}
	// add localpart of mailaddr and all substrings
	for _, mailaddr := range cert.EmailAddresses {
		idx := strings.Index(mailaddr, "@")
		if idx != -1 {
			localpart := mailaddr[0:idx]
			token[strings.ToLower(localpart)] = true
			for _, namepart := range strings.Split(localpart, ".") {
				_, unwanted := nameFiller[namepart]
				if unwanted == false {
					token[strings.ToLower(namepart)] = true
				}
			}
		}
	}
	cert.pubsearchToken = make([]string, len(token))
	idx := 0
	for key, _ := range token {
		cert.pubsearchToken[idx] = key
		idx++
	}

	// Sorting order: Group > User > Ext > Server > Pseudonym
	switch {
	case CertificateStats.FilterIsPseudonym(c):
		cert.Type = Pseudonym
	case CertificateStats.FilterIsGroup(c):
		cert.Type = Gruppe
	case CertificateStats.FilterIsExternal(c):
		cert.Type = Extern
	case CertificateStats.FilterIsNutzer(c):
		cert.Type = Benutzer
	case CertificateStats.And(SectigoPersonalFilter, NoCommonNameFilter)(c):
		cert.Type = Gruppe
	case CertificateStats.And(SectigoPersonalFilter, CertificateStats.Not(NoCommonNameFilter))(c):
		cert.Type = Benutzer
	default:
		cert.Type = Server
	}

	return cert
}

// JSONResult represents the json export structure of a single certificate
type JSONResult struct {
	Serial             string   `json:"serial"`
	HexSerial          string   `json:"hexserial"`
	SignatureAlgorithm string   `json:"sigalg"`
	Subject            string   `json:"subject"`
	CommonName         string   `json:"cn"`
	OrganizationalUnit string   `json:"ou"`
	KeyLength          int      `json:"keylength"`
	NotBeforeDuration  string   `json:"notbeforeduration"`
	NotBeforeDisplay   string   `json:"notbefore"`
	NotBeforeEpoch     int64    `json:"notbeforeepoch"`
	NotAfterDuration   string   `json:"notafterduration"`
	NotAfterDisplay    string   `json:"notafter"`
	NotAfterEpoch      int64    `json:"notafterepoch"`
	DNSNames           []string `json:"dnsnames,omitempty"`
	EmailAddresses     []string `json:"emailaddresses,omitempty"`
	IPAddresses        []string `json:"ipaddresses,omitempty"`
	CAGeneration       string   `json:"cageneration"`
	Type               string   `json:"type"`
	Profile            string   `json:"profile"`
	Expired            bool     `json:"expired"`
	Validity           string   `json:"valid"`
	Public             string   `json:"public"`
	FingerprintSHA1    string   `json:"fingerprintsha1"`
	FingerprintSHA256  string   `json:"fingerprintsha256"`
	FingerprintMD5     string   `json:"fingerprintmd5"`
}

func (c *SearchableCert) JSONResult(watchers map[int]*AttributeState) *JSONResult {
	now := time.Now()
	r := &JSONResult{
		Serial:             c.Serial,
		HexSerial:          c.HexSerial,
		SignatureAlgorithm: c.SignatureAlgorithm,
		Subject:            c.Subject,
		CommonName:         c.rawCertificate.Subject.CommonName,
		OrganizationalUnit: strings.Join(c.rawCertificate.Subject.OrganizationalUnit, "/"),
		KeyLength:          c.KeyLength,
		NotBeforeDuration:  humanreltime.German.Duration(c.NotBefore, now, humanreltime.Years, 2),
		NotBeforeDisplay:   c.NotBefore.Format(time.RFC1123Z),
		NotBeforeEpoch:     c.NotBefore.Unix(),
		NotAfterDuration:   humanreltime.German.Duration(c.NotAfter, now, humanreltime.Years, 2),
		NotAfterDisplay:    c.NotAfter.Format(time.RFC1123Z),
		NotAfterEpoch:      c.NotAfter.Unix(),
		DNSNames:           c.DNSNames,
		EmailAddresses:     c.EmailAddresses,
		IPAddresses:        c.IPAddresses,
		CAGeneration:       *c.CAGeneration,
		Type:               TypeToName(c.Type),
		Profile:            "",
		Expired:            c.NotAfter.Before(time.Now()),
		Validity:           ValidityToName(watchers[WatchValid].Get(c.Serial)),
		Public:             VisibilityToName(watchers[WatchVisibile].Get(c.Serial)),
		FingerprintMD5:     c.FingerprintMD5,
		FingerprintSHA1:    c.FingerprintSHA1,
		FingerprintSHA256:  c.FingerprintSHA256,
	}

	return r
}

type JSONShell struct {
	Results []*JSONResult `json:"results"`
}
