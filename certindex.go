package websearch

import (
	"sync"
)

// CertIndex provides a mapping between a string and a set of
// certificates in a thread-safe way
type CertIndex struct {
	certs map[string]map[string]*SearchableCert
	sync.RWMutex
}

func NewCertIndex() CertIndex {
	return CertIndex{
		certs: make(map[string]map[string]*SearchableCert),
	}
}

// Add cert to the set which is associated with key
func (ci *CertIndex) Add(key string, cert *SearchableCert) {
	ci.Lock()
	keyMap, present := ci.certs[key]
	if present {
		// assume Serial is unique
		keyMap[cert.Serial] = cert
	} else {
		ci.certs[key] = map[string]*SearchableCert{
			cert.Serial: cert,
		}
	}
	ci.Unlock()
}

// Get returns an array of all certificates for a key
func (ci *CertIndex) Get(key string) Searchresults {
	var matches Searchresults
	ci.RLock()
	serialMap, present := ci.certs[key]
	if present {
		matches = make(Searchresults, len(serialMap))
		var idx = 0
		for _, c := range serialMap {
			matches[idx] = c
			idx++
		}
	} else {
		matches = make(Searchresults, 0)
	}
	ci.RUnlock()
	return matches
}
