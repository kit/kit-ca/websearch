# KIT-CA Websearch


## To Do Feature List
### misc

* *H* legende
* *H* i18n
* *L* hilfetexte
* *M* infobutton
  * infos erzeugen. welche?
* *H* state in URL mitführen (next, …)
* *H* CI
* *H* downloads als PEM & DER
    * icon/font
* *H* Button:
    * Border bei hover
    * farbchange bei click
* *H* Barrierefreiheit
* *H* catch alle the errors!
* *H* keine externen komponenten
* *M* minify
* *L* deployment
* *L* Doku
* *L* favicon
* automatic scroll down to results
* sort email case-insensitive

### Filter

* *L* doppelklick => enable only
* *M* trefferzahlen an filter
* *H* Filter ein-/ausblendbar
* *M* nicht alle filter einer gruppe abwählbar
* *L* hover bei mobile

### shib

* *H* butten besser plazieren
* *M* im hintergrund state prüfen
* *H* sessiondauer erhöhen
