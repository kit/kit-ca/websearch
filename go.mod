module gitlab.kit.edu/kit/kit-ca/websearch

go 1.19

require (
	github.com/boltdb/bolt v1.3.1
	github.com/fsnotify/fsnotify v1.6.0
	github.com/gofrs/uuid v4.4.0+incompatible
	github.com/gorilla/mux v1.8.1
	github.com/gorilla/sessions v1.2.1
	github.com/hashicorp/go-memdb v1.3.4
	github.com/hreese/go-humanreltime v0.0.0-20170421160357-794c2e8d2412
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/mattn/go-colorable v0.1.13 // indirect
	golang.org/x/text v0.13.0
)

require (
	github.com/gorilla/securecookie v1.1.1 // indirect
	github.com/hashicorp/go-immutable-radix v1.3.1 // indirect
	github.com/hashicorp/golang-lru v1.0.2 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	gitlab.kit.edu/kit/kit-ca/lib/certificatestats v0.0.0-20250207165819-6c3517b36c80 // indirect
	golang.org/x/sys v0.16.0 // indirect
)
