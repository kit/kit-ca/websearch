package websearch

import (
	"errors"
	"fmt"
	"math/big"
)

var (
	serialG2First      big.Int
	serialG1Final      big.Int
	errorCannotConvert = errors.New("Unable to convert serial number to bigint")
	errorUnknownCA     = errors.New("Unable to determine CA generation")
	RedirTemplates     = map[string]string{
		"getcert":     "https://pki.pca.dfn.de/%s/cgi-bin/pub/pki?cmd=send_email_cert&type=email&dataType=CERTIFICATE&key=%s",
		"installcert": "https://pki.pca.dfn.de/%s/cgi-bin/pub/pki?cmd=getcert&type=CERTIFICATE&key=%s",
		"viewcert":    "https://pki.pca.dfn.de/%s/cgi-bin/pub/pki?cmd=viewCert;dataType=CERTIFICATE;key=%s",
	}
)

func init() {
	// this is the serial of the first certificate of KIT-CA G2
	serialG2First.SetString("8926168349745120614054526923", 10)
	// this is the serial of the last certificate of KIT-CA G1
	serialG1Final.SetString("9999999999999999999999999999", 10) // TODO: anpassen sobald bekannt

}

func GetIssuer(serial string, ccache *CertCache) (string, error) {
	// convert serial to integer
	var sernum big.Int
	_, ok := sernum.SetString(serial, 10)
	if !ok {
		return "", errorCannotConvert
	}
	// alte CA (kurze nummern, serial kleiner als erstes g2)
	if len(serial) == 8 || len(serial) == 14 || sernum.Cmp(&serialG2First) < 1 {
		return kitcag1, nil
	}
	// neue CA (seriennummer größer als letztes g1)
	if sernum.Cmp(&serialG1Final) == 1 {
		return kitcag2, nil
	}
	// check certificate cache
	fromcache := ccache.Get(serial)
	if fromcache == nil {
		// don't know? assume G2
		return kitcag2, nil
		//return "", errorUnknownCA
	}
	return *fromcache.CAGeneration, nil
}

func BuildCertificateLink(template, ca, serial string) string {
	return fmt.Sprintf(template, ca, serial)
}
