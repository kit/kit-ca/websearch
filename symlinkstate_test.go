package websearch

import (
	"fmt"
	"github.com/k0kubun/pp"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"testing"
	"time"
)

// CopyFile copies the contents from src to dst using io.Copy.
// If dst does not exist, CopyFile creates it with permissions perm;
// otherwise CopyFile truncates it before writing.
// Source: https://codereview.appspot.com/152180043/
func CopyFile(src, dst string, perm os.FileMode) error {
	in, err := os.Open(src)
	if err != nil {
		return err
	}
	defer in.Close()
	out, err := os.OpenFile(dst, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, perm)
	if err != nil {
		return err
	}
	defer func() {
		if err := out.Close(); err != nil {
			return
		}
	}()
	_, err = io.Copy(out, in)
	return err
}

func meh() {
	log.Print()
	pp.Print()
	time.Sleep(1 * time.Millisecond)
}

const (
	DirA     = "All"
	DirB     = "Valid"
	DirC     = "Invalid"
	DirArch  = ".archive"
	NUMFILES = 10
)

var (
	basedir string
)

func init() {
	//rand.Seed(time.Now().UTC().UnixNano())
	rand.Seed(23)

	// create testing environment
	var err error
	basedir, err = ioutil.TempDir("", "____")
	if err != nil {
		log.Fatal(err)
	}
	//defer os.RemoveAll(basedir)
}

func TestCertArchiveWatcher(t *testing.T) {
	archdir := filepath.Join(basedir, DirArch)
	err := os.MkdirAll(archdir, 0700)
	if err != nil {
		t.Fatal(err)
	}
	CopyFile("_testcerts/start/.archive/cert-8961318184135709443988218059.pem", filepath.Join(archdir, "cert-8961318184135709443988218059.pem"), 0600)

	initialBatchDone := make(chan bool, 1)
	cc := CertArchiveWatcher(archdir, initialBatchDone)
	time.Sleep(100 * time.Millisecond)
	CopyFile("_testcerts/start/.archive/cert-8961368082574900309097365394.pem", filepath.Join(archdir, "cert-8961368082574900309097365394.pem"), 0600)
	time.Sleep(100 * time.Millisecond)
	CopyFile("_testcerts/start/.archive/cert-8961409163933414819885512221.pem", filepath.Join(archdir, "cert-8961409163933414819885512221.pem"), 0600)

	time.Sleep(1000 * time.Millisecond)
	pp.Print(cc)
}

func TestNewAttributeState(t *testing.T) {
	var err error
	somedata := []byte{}
	serials := make([]string, NUMFILES)

	// create testing directories
	for _, name := range []string{DirA, DirB, DirC} {
		os.MkdirAll(filepath.Join(basedir, name), 0700)
		if err != nil {
			t.Fatal(err)
		}
	}
	// create files in DirA
	for i := 0; i < 10; i++ {
		serial := fmt.Sprintf("%016d", rand.Int63())
		fname := fmt.Sprintf("foobar-%s.pem", serial)
		serials[i] = serial
		filename := filepath.Join(basedir, DirA, fname)
		ioutil.WriteFile(filename, somedata, 0666)
		if err != nil {
			t.Fatal(err)
		}
		err = os.Symlink(filename, filepath.Join(basedir, DirC, fname))
		if err != nil {
			t.Fatal(err)
		}
	}
	// create watcher
	as := NewAttributeState([]WatchForSymlinkChange{
		//NewAttributeState([]WatchForSymlinkChange{
		{filepath.Join(basedir, DirA), 0x01},
		{filepath.Join(basedir, DirB), 0x02},
		{filepath.Join(basedir, DirC), 0x04},
	})

	// do something observable
	afiles, _ := ioutil.ReadDir(filepath.Join(basedir, DirA))
	for _, filename := range afiles {
		// create symlinks from DirA to DirB
		var source, destination string
		source = filepath.Join(basedir, DirA, filename.Name())
		destination = filepath.Join(basedir, DirB, filename.Name())
		//log.Printf("XXXXX %s -> %s XXXXX", source, destination)
		err = os.Symlink(source, destination)
		if err != nil {
			t.Fatal(err)
		}
	}

	// remove links from DirC
	cfiles, _ := ioutil.ReadDir(filepath.Join(basedir, DirC))
	for _, filename := range cfiles {
		if rand.Intn(100) < 50 {
			err = os.Remove(filepath.Join(basedir, DirC, filename.Name()))
			if err != nil {
				t.Fatal(err)
			}
		}
	}
	time.Sleep(1000 * time.Millisecond)
	for _, serial := range serials {
		log.Printf("%s: %04b", serial, as.Get(serial))
	}
	pp.Print(as)
}
